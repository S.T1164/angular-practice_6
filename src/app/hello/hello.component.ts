import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder,Validators} from '@angular/forms';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  title : string;
  message : string;
  

  constructor() { }

  ngOnInit(): void {

    this.title = 'Hello-app';
    this.message = 'One,Two,Three,Four,Five'
   

  }
}
